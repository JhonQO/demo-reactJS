// (1)---------- Filter and  Lista ------------
var ComboBoxSearchOnTimeReal = React.createClass({

    getInitialState: function(){
        return { searchString: '' };
    },
    handleChange: function(e){
        this.setState({searchString:e.target.value});
    },
    render: function() {
        var libraries = this.props.items,
            searchString = this.state.searchString.trim().toLowerCase();
        if(searchString.length > 0){
            // We are searching. Filter the results.
            libraries = libraries.filter(function(l){
                return l.name.toLowerCase().match( searchString );
            });
        }
        return (
        		<div className="col-xs-6">
                    <input type="text" value={this.state.searchString} onChange={this.handleChange} placeholder="Type here" />
                    <ul> 
                        { libraries.map(function(l){
                            return <li key={l.id}>{l.name} <a href={l.url}>{l.url}</a></li>
                        }) }
                    </ul>
                </div> 
               );
    }
});

                                                                                                                                                             
var libraries = [

{id:'0',name: 'Backbone.js', url: 'http://documentcloud.github.io/backbone/'},
{id:'1',name: 'AngularJS', url: 'https://angularjs.org/'},
{id:'2',name: 'jQuery', url: 'http://jquery.com/'},
{id:'3',name: 'Prototype', url: 'http://www.prototypejs.org/'},
{id:'4',name: 'React', url: 'http://facebook.github.io/react/'},
{id:'5',name: 'Ember', url: 'http://emberjs.com/'},
{id:'6',name: 'Knockout.js', url: 'http://knockoutjs.com/'},
{id:'7',name: 'Dojo', url: 'http://dojotoolkit.org/'},
{id:'8',name: 'Mootools', url: 'http://mootools.net/'},
{id:'9',name: 'Underscore', url: 'http://documentcloud.github.io/underscore/'},
{id:'10',name: 'Lodash', url: 'http://lodash.com/'},
{id:'11',name: 'Moment', url: 'http://momentjs.com/'},
{id:'12',name: 'Express', url: 'http://expressjs.com/'},
{id:'13',name: 'Koa', url: 'http://koajs.com/'}

];

//estas clases renderizaremos al final, todo en un solo grupo


//*************************************************************************************************************************************


//(2)----- Uso de Eventos- Observe como el el Service(elemento hijo) hace uso del metodo de ServiceChooser (elemento Padre)


var Service = React.createClass({

 getInitialState: function(){
     return { active: false };
 },

 clickHandler: function (){
     var active = !this.state.active;
     this.setState({ active: active });
     // Notify the ServiceChooser, by calling its addTotal method
     this.props.addTotal( active ? this.props.price : -this.props.price );
 },
 render: function(){
     return ( <p className={ this.state.active ? 'active' : '' } onClick={this.clickHandler}>
                 {this.props.name} <b>${this.props.price.toFixed(2)}</b>
             </p>
            );
 }
});


var ServiceChooser = React.createClass({
 getInitialState: function(){
     return { total: 0 };
 },
 addTotal: function( price ){
     this.setState( { total: this.state.total + price } );
 },
 render: function() {
     var self = this;
     var services = this.props.items.map(function(s){
         return <Service name={s.name} price={s.price} active={s.active} addTotal={self.addTotal}  />;
     });
     return <div className="col-xs-6">
                 <h1>Our services</h1>
                 <div id="services">
                     {services}
                     <p id="total">Total <b>${this.state.total.toFixed(2)}</b></p>
                 </div>
             </div>;
 }
});


var services = [
 { name: 'Web Development', price: 300 },
 { name: 'Design', price: 400 },
 { name: 'Integration', price: 250 },
 { name: 'Training', price: 220 }
];





//************************************************************************************************

//(3) ------- Cargar Imagenes

var Picture = React.createClass({
    clickHandler: function(){
        this.props.onClick(this.props.id);
    },
    render: function(){
        var cls = 'picture ' + (this.props.favorite ? 'favorite' : '');
        return (
            <div className={cls} onClick={this.clickHandler}>
                <img src={this.props.src} width="200" title={this.props.title} />
            </div>
        );
    }
});

var PictureList = React.createClass({
    getInitialState: function(){
        return { pictures: [], favorites: [] };
    },
    componentDidMount: function(){
        var self = this;
        var url = 'https://api.instagram.com/v1/media/popular?client_id=' + this.props.apiKey + '&callback=?';

        $.getJSON(url, function(result){
            if(!result || !result.data || !result.data.length){
                return;
            }
            var pictures = result.data.map(function(p){
                return { 
                    id: p.id, 
                    url: p.link, 
                    src: p.images.low_resolution.url, 
                    title: p.caption ? p.caption.text : '', 
                    favorite: false 
                };
            });

            // Update the component's state. This will trigger a render.
            // Note that this only updates the pictures property, and does
            // not remove the favorites array.
            self.setState({ pictures: pictures });
        });
    },
    pictureClick: function(id){

        // id holds the ID of the picture that was clicked.
        // Find it in the pictures array, and add it to the favorites

        var favorites = this.state.favorites,
            pictures = this.state.pictures;

        for(var i = 0; i < pictures.length; i++){
           if(pictures[i].id == id) { // Find the id in the pictures array                  
                if(pictures[i].favorite){
                    return this.favoriteClick(id);
                }
                // Add the picture to the favorites array, and mark it as a favorite:
                favorites.push(pictures[i]);
                pictures[i].favorite = true;
                break;
            }
        }
        // Update the state and trigger a render
        this.setState({pictures: pictures, favorites: favorites});
    },

    favoriteClick: function(id){
        // Find the picture in the favorites array and remove it. After this, 
        // find the picture in the pictures array and mark it as a non-favorite.
        var favorites = this.state.favorites,
            pictures = this.state.pictures;

        for(var i = 0; i < favorites.length; i++){
            if(favorites[i].id == id) break;
        }
        // Remove the picture from favorites array
        favorites.splice(i, 1);
        for(i = 0; i < pictures.length; i++){
            if(pictures[i].id == id) {
                pictures[i].favorite = false;
                break;
            }
        }
        // Update the state and trigger a render
        this.setState({pictures: pictures, favorites: favorites});
    },

    render: function() {
        var self = this;
        var pictures = this.state.pictures.map(function(p){
            return <Picture id={p.id} src={p.src} title={p.title} favorite={p.favorite} onClick={self.pictureClick} />
        });

        if(!pictures.length){
            pictures = <p>Loading images..</p>;
        }

        var favorites = this.state.favorites.map(function(p){
            return <Picture id={p.id} src={p.src} title={p.title} favorite={true} onClick={self.favoriteClick} />
        });

        if(!favorites.length){
            favorites = <p>Click an image to mark it as a favorite.</p>;
        }

        return (

            <div className="col-xs-6">
                <h1>Popular Instagram pics</h1>
                <div className="pictures"> {pictures} </div>
                    
                <h1>Your favorites</h1>
                <div className="favorites"> {favorites} </div>
            </div>

        );
    }
});





// Esta Clase es el que engloba las clases a renderizar.

var BodyElements = React.createClass({
	render : function(){
		return (
				<div className="form-inline">
					  	<ComboBoxSearchOnTimeReal items={ this.props.itemsCombo } />,
					  	<ServiceChooser items={ this.props.itemsChooser } />,
						<PictureList  apiKey ={this.props.apiKey}/>
				</div>
		);
	}
	
});

// renderizamos.

ReactDOM.render(
	    <BodyElements itemsCombo={ libraries } itemsChooser={ services }  apiKey="642176ece1e7445e99244cec26f4de1f" />,
	    document.getElementById('secction')
	);
