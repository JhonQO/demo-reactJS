
// Hay Dos maneras de definir las clases:
// (1) var Avatar = React.createClass
// (2) class LikeButton extends React.Component 
// En ambos casos observe como se creen los metodos.


class LikeButton extends React.Component {
  constructor() {
    super();
    this.state = {
      liked: false
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState({liked: !this.state.liked});
  }
  render() {
    const text = this.state.liked ? 'liked' : 'haven\'t liked';
    return (
      <div onClick={this.handleClick}>
        You {text} this. Click to toggle.
      </div>
    );
  }
}



var Avatar = React.createClass({
	  render: function() {
	    return (
	      <div>
	        <PagePic pagename={this.props.pagename} />
	        <PageLink pagename={this.props.pagename} />
	      </div>
	    );
	  }
	});

	var PagePic = React.createClass({
	  render: function() {
	    return (
	      <img src={'https://graph.facebook.com/' + this.props.pagename + '/picture'} />
	    );
	  }
	});

	var PageLink = React.createClass({
	  render: function() {
	    return (
	      <a href={'https://www.facebook.com/' + this.props.pagename}>
	        {this.props.pagename}
	      </a>
	    );
	  }
	});



class ContentLikeButton extends React.Component {s
	constructor(){
		super();
	}
	render () {
		return (
				<div>
					<h3> Option to CLick </h3>
					<LikeButton/>,
					 <Avatar pagename={this.props.pagename} />
				</div>
		);
	}
}

//	

ReactDOM.render(
  <ContentLikeButton pagename ="Engineering"/>,
  document.getElementById('secction2')
);