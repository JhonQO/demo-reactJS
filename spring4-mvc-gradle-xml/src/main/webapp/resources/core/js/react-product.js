
class Product extends React.Component {

  constructor (){
	  super();
    this.state = {
    		qty:0
    	    };
    this.buy=this.buy.bind(this);
    this.show=this.show.bind(this);
  }
  buy(){
    this.setState({qty:this.state.qty+1});
    this.props.handleTotal(this.props.price);
  }
  show(){
    this.props.handleShow(this.props.name);
  }
  render() {
    return ( 
      <div>
      <p>{this.props.name} - ${this.props.price}</p>
      <button onClick={this.buy}>buy</button>
      <button onClick={this.show}>show</button>
      <h3>Qty:{this.state.qty} items(s)</h3>
      <hr/>
      </div>
      
    );
  }
}

class Total extends React.Component{
	constructor(){
		super();
	}
  render () {
    return (
        <div>
          <h3> Total Cash: $ {this.props.total}</h3>
        </div>
      );
  }
}

class ProductForm extends React.Component{
	constructor(props){
		super(props);
		this.state = {
				name :'',
				price:''
		}
		this.handleNameChange= this.handleNameChange.bind(this);
		this.handlePriceChange= this.handlePriceChange.bind(this);
		this.submit= this.submit.bind(this);
	
	}
	handleNameChange (e){
		this.setState({name:e.target.value});
	}
	handlePriceChange (e){
		this.setState({price:e.target.value});
	}
	submit(e){
		
		 e.preventDefault();
		
		var name = this.state.name.trim();
		var price = this.state.price.trim()
		if(!name || !price){
				return;
		}
			
		var product = {
	      name: name,
	      price: parseInt(price)
	    } 
		
		this.props.handleCreate(product);
		this.setState({name:'', price:''});
	   
	 }
	 render(){
	    return(
	      <form onSubmit={this.submit}>
	        <input type="text" placeholder="product Name" ref="name" onChange={this.handleNameChange}/> -
	        <input type="text" placeholder="product price" ref="price" onChange={this.handlePriceChange}/>
	        <br/>
	        <br/>
	        <button> Create Product</button>
	        <hr/>
	      </form>
	      );
	  }
}

 class ProductList extends React.Component{
  constructor(){
	  super();
	  this.state = {
	      total:0,
	      productList: [
	        {id:1, name:"Android", price:121},
	        {id:2, name:"Aple", price:123},
	        {id:3, name:"Nokia", price:65}]
	      
	    };
	  this.createProduct=this.createProduct.bind(this);
	  this.calculateTotal=this.calculateTotal.bind(this);
	  this.showProduct=this.showProduct.bind(this);
  }
  createProduct (product){
    this.setState({
      productList: this.state.productList.concat(product)
    });
  }
  
  calculateTotal (price){
    this.setState({total:this.state.total+price});
  }
  showProduct (name){
    alert("You select " +name)
  }
  render (){
    var component = this;
    var products=this.state.productList.map(function(product){
      return (<Product name={product.name} price={product.price} key={product.id}
       handleShow={component.showProduct} 
       handleTotal={component.calculateTotal}/>
       );
    });
    return (
      <div className="col-xs-6">
      <ProductForm handleCreate={this.createProduct}/>
       {products}
       <Total total={this.state.total}/>
      </div>
      )
  }
};



 ReactDOM.render( <ProductList/> ,
 document.getElementById("product"));
//
//'use strict';
//Object.defineProperty(exports, "__esModule", {
//  value: true
//});
//exports.default = 'ProductList';
//module.exports = exports['default'];