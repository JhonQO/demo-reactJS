var Option = React.createClass({
	render: function(){
		return(
			<option value={this.props.value}>{this.props.description}</option>
		);
	}
});

var DropDown = React.createClass({
	getInitialState: function(){
		return { value: 0 }
	},
	render: function(){
		var options = this.props.myData.map(function(opt){
			return(
					<Option value={opt.value} description={opt.descrip} key={opt.value}/>
				);
		})
		
		return (
			<select multiple={false} value={this.state.value}>
			 {options}
			</select>
		)
	}
})

var ClientProgram = React.createClass({
	getInitialState : function(){
		return { 
			mensaje:"Hola Peru",
			dataClientProgram:[ ]
		} 
	},
	componentDidMount: function() {
		var myURL= "/demoProyects/resources/core/json/data.json"; // deberia ser
																// llamada al
																// servicio.
		// this.serverRequest = $.get({url:myURL,headers: { "dataType":
		// "json"}}, function (result) {
		this.serverRequest = $.getJSON( "/demoProyects/resources/core/json/data.json", function (result) { 
			console.log(result)
			  this.setState({
				  dataClientProgram:result
			  });
		 }.bind(this));
	},

	render : function(){
		return (
				<div className="form-group">
					<label className="col-sm-2 control-label">Client Program</label>
					<div className="col-md-6">
						<p> {this.state.mensaje} </p> 
						<DropDown myData={this.state.dataClientProgram}/>

					</div>
				</div>
				
		);
	}
});


 var ContentForm = React.createClass({
		render: function(){
			return(
					<div className="col-xs-6">
						<form>
							<ClientProgram />
						</form>
					
					</div>
					);
		}
	});
	 
	 
	 
ReactDOM.render(<ContentForm />,
	document.getElementById('secction3')
	);