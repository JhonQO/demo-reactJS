<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/core/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/core/css/myReactJS.css" />

</head>
<body>

	<div class="container">
		<div class="container-fluid">

			<div id="header">
				<!-- This element's contents will be replaced with your component. -->
			</div>
			
			<div>
				<div id="secction"> </div>
			</div>
			<div id="secction2"> 
			</div>

			<script src="https://unpkg.com/react@15.3.1/dist/react.js"></script>
    		<script src="https://unpkg.com/react-dom@15.3.1/dist/react-dom.js"></script>
    		<script src="https://unpkg.com/babel-core@5.8.38/browser.min.js"></script>
    		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
  
<!-- 			<script -->
<!-- 				src="https://facebook.github.io/react/js/jsfiddle-integration-babel.js"></script> -->
			
			<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/reactjs-menu.js"></script>
			<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/reactjs-Filter-Events-Listar-Images.js"></script>
						
			<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/reactjs-clases.js"></script>		
		</div>
	</div>
</body>
</html>