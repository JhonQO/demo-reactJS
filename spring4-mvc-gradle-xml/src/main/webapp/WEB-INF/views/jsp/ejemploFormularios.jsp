<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <title>Insert title here</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/core/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/core/css/myReactJS.css" />
</head>


<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

<body>

	<div class="container">
		<div class="container-fluid">

			<div id="header">
				<!-- This element's contents will be replaced with your component. -->
			</div>

			<div className="form-inline">
				<div id="secction3"></div>
				<div id="product"></div>
				<div id="comment"></div>
				<div id="myView"></div>
			</div>


		</div>
	</div>

	<script src="https://unpkg.com/react@15.3.1/dist/react.js"></script>
	<script src="https://unpkg.com/react-dom@15.3.1/dist/react-dom.js"></script>
	<script src="https://unpkg.com/babel-core@5.8.38/browser.min.js"></script>
 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/remarkable@1.6.2/dist/remarkable.min.js"></script>
    
	<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/react-myFormulario.js"></script>
	<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/react-product.js"></script>
	<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/react-comment.js"></script>
	<script type="text/babel" src="${pageContext.request.contextPath}/resources/core/js/reactjs-menu.js"></script>
					

</body>
</html>