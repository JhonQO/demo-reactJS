package com.jhonqo.reactjs.web;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {

	private final Logger logger = LoggerFactory.getLogger(WelcomeController.class);


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Map<String, Object> model) {

		logger.debug("index() is executed!");

		model.put("title", "Pruebas de ReacJS");
		model.put("msg", "Welcome");
		
		return "index";
	}
	@RequestMapping(value = "/reactjs/form", method = RequestMethod.GET)
	public String startEjemplosReactJS() {

		logger.debug("startEjemplosReactJS() is executed!");
		
		return "ejemploFormularios" ; //"index";
	}
	@RequestMapping(value = "/reactjs/others", method = RequestMethod.GET)
	public String startEjemplosReactJSOthers() {

		logger.debug("startEjemplosReactJS() is executed!");
		
		return "ejemplosVariosReactJS" ; //"index";
	}

	

}